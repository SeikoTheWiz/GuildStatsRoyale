<?php

$API_token = 'enter your API Key';

/******************
 *  API requests  *
 ******************/

$opts = array(
  'http'=>array(
    'method'=>'GET',
	'header'=>'auth: ' . $API_token,
	'timeout'=>30
  )
);

$context = stream_context_create($opts);

$clan_id = '2P0V8GL';
$player_id = '22VQC09PL'; // 2G8GV022G
 
$clan_json_response = file_get_contents('https://api.royaleapi.com/clan/' . $clan_id, true, $context);
$clan_response = json_decode($clan_json_response);

$warlog_json_response = file_get_contents('https://api.royaleapi.com/clan/' . $clan_id . '/warlog', true, $context);
$warlog_response = json_decode($warlog_json_response);


$player_json_response = file_get_contents('https://api.royaleapi.com/player/' . $player_id, true, $context);
$player_response = json_decode($player_json_response);


/*********************************
 *  Request responses treatment  *
 *********************************/
 
// Get clan members list
foreach($clan_response->members as $member)	{
	$clan_members[] = strtolower($member->name);
	$clan_war_table[$member->tag] = array('Name' => $member->name,'CardsEarned' => '','Game 1' => '','Game 2' => '');
}

// War clan analysis
$war = $warlog_response[0];
foreach($war->participants as $part)	{
	$clan_war_table[$part->tag]['CardsEarned'] = $part->cardsEarned;
	switch ($part->battlesPlayed) {
		case 1:
			$clan_war_table[$part->tag]['Game 1'] = $part->wins;
			break;
		case 2:
			if ($part->wins == 2) {
				$clan_war_table[$part->tag]['Game 1'] = 1;
				$clan_war_table[$part->tag]['Game 2'] = 1;
			} elseif ($part->wins == 2) {
				$clan_war_table[$part->tag]['Game 1'] = 1;
				$clan_war_table[$part->tag]['Game 2'] = 0;
			} else {
				$clan_war_table[$part->tag]['Game 1'] = 0;
				$clan_war_table[$part->tag]['Game 2'] = 0;
			}
			break;
	}
}

array_multisort($clan_members, SORT_ASC, SORT_STRING, $clan_war_table);

// Display table
echo('<table>');
foreach ($clan_war_table as $part) {
	echo('
	<tr>');
	echo('
		<td>' . $part['Name'] . '</td>
		<td>' . $part['CardsEarned'] . '</td>
		<td>' . $part['Game 1'] . '</td>
		<td>' . $part['Game 2'] . '</td>
	</tr>');
}
echo('</table>');

?>