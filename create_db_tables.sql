CREATE DATABASE ClashRoyale CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE ClashRoyale;

-- Clan War Table creation

CREATE TABLE ClanWar(
createdDate DOUBLE NOT NULL,
seasonNumber DOUBLE NOT NULL,
PRIMARY KEY (createdDate));

CREATE TABLE standings(
createdDate DOUBLE NOT NULL,
tag VARCHAR(50) NOT NULL,
name VARCHAR(50) NOT NULL,
participants DOUBLE NOT NULL,
battlesPlayed DOUBLE NOT NULL,
wins DOUBLE NOT NULL,
crowns DOUBLE NOT NULL,
warTrophies DOUBLE NOT NULL,
warTrophiesChange DOUBLE NOT NULL,
PRIMARY KEY (createdDate, tag),
FOREIGN KEY (createdDate) REFERENCES ClanWar(createdDate)
);

CREATE TABLE participants(
createdDate DOUBLE NOT NULL,
tag VARCHAR(50) NOT NULL,
name VARCHAR(50) NOT NULL,
cardsEarned DOUBLE NOT NULL,
battlesPlayed DOUBLE NOT NULL,
wins DOUBLE NOT NULL,
PRIMARY KEY (createdDate, tag),
FOREIGN KEY (createdDate) REFERENCES ClanWar(createdDate)
);

CREATE TABLE ClanWarBadges(
createdDate DOUBLE NOT NULL,
tag VARCHAR(50) NOT NULL,
name VARCHAR(50) NOT NULL,
category VARCHAR(50) NOT NULL,
id DOUBLE NOT NULL,
image VARCHAR(100) NOT NULL,
PRIMARY KEY (createdDate, tag),
FOREIGN KEY (createdDate, tag) REFERENCES standings(createdDate, tag)
);

-- Battles

CREATE TABLE Cards(
name VARCHAR(50) NOT NULL,
rarity VARCHAR(50) NOT NULL,
icon VARCHAR(100) NOT NULL,
c_key VARCHAR(50) NOT NULL,
elixir DOUBLE NOT NULL,
c_type VARCHAR(50) NOT NULL,
arena DOUBLE NOT NULL,
description VARCHAR(100) NOT NULL,
id DOUBLE NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE DeckLists(
id DOUBLE NOT NULL,
endDate DOUBLE NOT NULL,
slot1 DOUBLE NOT NULL,
slot2 DOUBLE NOT NULL,
slot3 DOUBLE NOT NULL,
slot4 DOUBLE NOT NULL,
slot5 DOUBLE NOT NULL,
slot6 DOUBLE NOT NULL,
slot7 DOUBLE NOT NULL,
slot8 DOUBLE NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE DeckStats(
id DOUBLE NOT NULL,
battleTime DOUBLE NOT NULL,
tag VARCHAR(50) NOT NULL,
name VARCHAR(50) NOT NULL,
teamCrowns DOUBLE NOT NULL,
opponentCrowns DOUBLE NOT NULL,
level_s1 DOUBLE NOT NULL,
level_s2 DOUBLE NOT NULL,
level_s3 DOUBLE NOT NULL,
level_s4 DOUBLE NOT NULL,
level_s5 DOUBLE NOT NULL,
level_s6 DOUBLE NOT NULL,
level_s7 DOUBLE NOT NULL,
level_s8 DOUBLE NOT NULL,
PRIMARY KEY (id, battleTime, tag),
FOREIGN KEY (id) REFERENCES DeckLists(id)
);