<?php

$API_token = 'enter your API Key';

/* 
Remaining : 

2. Vérfier si la fin de jour de guerre est différent du la fin du jour de guerre stocké en bdd (warEndTimeDB), si oui vider les tables dans l ordre suivant : stats puis winner count puis deck liste

3. Faire la requete pour l affichage des decks gagnants et leurs stats
 */

/*************************
 *  DataBase Connection  *
 *************************/
 
$sql_serveur = '127.0.0.1';
$sql_username = 'root';
$sql_password = '';
$sql_dbname = 'ClashRoyale';
 
$conn = new mysqli($sql_serveur,$sql_username,$sql_password,$sql_dbname);

// Check connection
if ($conn->connect_errno) {
    printf("Connection failed: %s\n", $conn->connect_error);
    exit();
}

/***********************************
 *  DataBase Preliminary requests  *
 ***********************************/
 
// Get Cards list
$query = 'SELECT id FROM Cards';
$query_result = $conn->query($query);

$cardListDB = array();
while($row = $query_result->fetch_array(MYSQLI_ASSOC)) {
	$cardListDB[] = $row['id'];
}
$query_result->free();

// var_dump($cardListDB);
// echo('<br>');

// Get warEndTime store in database
$query = 'SELECT min(endDate) AS warEndTimeDB FROM DeckLists';
$query_result = $conn->query($query);

while($row = $query_result->fetch_array(MYSQLI_ASSOC)) {
	$warEndTimeDB = $row['warEndTimeDB'];
}
$query_result->free();

// var_dump($warEndTimeDB);
// echo('<br>');

// Get today Decks list
$query = 'SELECT * FROM DeckLists ORDER BY id';
$query_result = $conn->query($query);

$deckListDB = array();
$id_NewDeck = 0;
while($row = $query_result->fetch_array(MYSQLI_ASSOC)) {
	$deckListDB[$row['id']] = [$row['slot1'], $row['slot2'], $row['slot3'], $row['slot4'], $row['slot5'], $row['slot6'], $row['slot7'], $row['slot8']];
	$id_NewDeck = max($id_NewDeck,$row['id']);
}

$query_result->free();

// var_dump($deckListDB);
// echo('<br>');

// var_dump($id_NewDeck);
// echo('<br>');

// Get today winners list
$query = 'SELECT tag, COUNT(*) AS wins FROM DeckStats GROUP BY tag';
$query_result = $conn->query($query);

$winnersListDB = array();
$winnersList_nbrWinsDB = array();
while($row = $query_result->fetch_array(MYSQLI_ASSOC)) {
	$winnersListDB[] = $row['tag'];
	$winnersList_nbrWinsDB[$row['tag']] = $row['wins'];
}

$query_result->free();

// var_dump($winnersListDB);
// echo('<br>');

// var_dump($winnersList_nbrWinsDB);
// echo('<br>');

// Get today battle time list
$query = 'SELECT battleTime FROM DeckStats';
$query_result = $conn->query($query);

$battleTimeListDB = array();
while($row = $query_result->fetch_array(MYSQLI_ASSOC)) {
	$battleTimeListDB[] = $row['battleTime'];
}

$query_result->free();

// var_dump($battleTimeListDB);
// echo('<br>');

/******************
 *  API requests  *
 ******************/

$opts = array(
  'http'=>array(
    'method'=>'GET',
    'header'=>'auth: ' . $API_token,
	'timeout'=>30
  )
);

$context = stream_context_create($opts);

$clan_id = '2P0V8GL';


// Get current clan war winners list if it is war day
$war_json_response = file_get_contents('https://api.royaleapi.com/clan/' . $clan_id . '/war', true, $context);
$war_response = json_decode($war_json_response);

// Request responses treatment
$winners_list = '';
$winners_list_name = '';
$newWinnerCount = 0;
$query_Cards = '';
$query_DeckLists = '';
$query_DeckStats = '';

if (strcmp($war_response->state,'warDay') == 0) {
	foreach($war_response->participants as $part) {
		$new_win = 1;
		if (in_array($part->tag,$winnersListDB)) {
			if ($part->wins == $winnersList_nbrWinsDB[$part->tag]) {
				$new_win = 0; // Already in database
			}
		}
		
		if ($new_win == 1) {
			if ($part->wins >= 1) {
				if (!empty($winners_list)) {
					$winners_list =  $winners_list . ',';
				}
				$winners_list =  $winners_list . $part->tag;
				$winners_list_name =  $winners_list_name . $part->name . ' | ';
				$newWinnerCount = $newWinnerCount + 1;
			}
		}
	}
	
	echo($winners_list_name . '<br><br>');
	
	if ($newWinnerCount > 0) {
		// New API request to get all winners battles
		$win_players_json_response = file_get_contents('https://api.royaleapi.com/player/' . $winners_list . '/battles', true, $context); 
		$win_players_response = json_decode($win_players_json_response);

		// Request responses treatment
		$cardListNew = array();
		$deckListNew = array();
		if ($newWinnerCount == 1) { // If only one item in response encapsulate into array
			$win_players_response = array($win_players_response);
		}
		foreach($win_players_response as $part) {
			foreach($part as $battle) {
				if ((strcmp($battle->type,'clanWarWarDay') == 0) && ($battle->winner >= 1) && (($war_response->warEndTime - $battle->utcTime) < 86400) && (!in_array($battle->utcTime,$battleTimeListDB))) { // New war day battle found
					$deckList = '<br>' . $battle->team[0]->name . ' : ';
					
					$deck = array();
					$cardLevel = array();
					foreach($battle->team[0]->deck as $card) {
						$deck[] = $card->id;
						$cardLevel[] = $card->level;
						$deckList = $deckList . $card->name . ' | ';
						if ((!in_array($card->id,$cardListDB)) && (!in_array($card->id,$cardListNew))) {
							$cardListNew[] = $card->id;
							if (!empty($query_Cards)) {
								$query_Cards = $query_Cards . ', ';
							}
							$query_Cards = $query_Cards . '("' . mysql_real_escape_string($card->name) . '", "' . $card->rarity . '", "' . $card->icon . '", "' . mysql_real_escape_string($card->key) . '", ' . $card->elixir . ', "' . $card->type . '", ' . $card->arena . ', "' . mysql_real_escape_string($card->description) . '", ' . $card->id . ')';
						}
					}
					array_multisort($deck,$cardLevel);
					
					if (in_array($deck,$deckListDB)) { 	// Same deck as an other player deck already stored in database
						$id_CurrDeck = array_search($deck,$deckListDB);
					} elseif (in_array($deck,$deckListNew)) { 	// Same deck as an other player deck found with the same API request
						$id_CurrDeck = array_search($deck,$deckListNew);
					} else { // New deck found
						$id_NewDeck = $id_NewDeck + 1;
						$id_CurrDeck = $id_NewDeck;
						if (!empty($query_DeckLists)) {
							$query_DeckLists = $query_DeckLists . ', ';
						}
						$query_DeckLists = $query_DeckLists . '(' . $id_NewDeck . ', ' . $war_response->warEndTime  . ', ' . implode(', ',$deck) . ')';
						$deckListNew[$id_NewDeck] = $deck;
					}

					if (!empty($query_DeckStats)) {
						$query_DeckStats = $query_DeckStats . ', ';
					}
					$query_DeckStats = $query_DeckStats . '(' . $id_CurrDeck . ', ' . $battle->utcTime  . ', "' . $battle->team[0]->tag  . '", "' . mysql_real_escape_string($battle->team[0]->name)  . '", ' . $battle->teamCrowns  . ', ' . $battle->opponentCrowns  . ', ' . implode(', ',$cardLevel) . ')';
					
					echo($deckList);
				}
			}
		}
	}
}


// Clan war update

if (!empty($query_Cards)) {
	$query_Cards = 'INSERT INTO Cards VALUES ' . $query_Cards;
	echo('<br><br>' . $query_Cards);
	
	if ($conn->query($query_Cards) === TRUE) {
		echo('<br>Cards table successfully updated.');
	}
}

if (!empty($query_DeckLists)) {
	$query_DeckLists = 'INSERT INTO DeckLists VALUES ' . $query_DeckLists;
	echo('<br><br>' . $query_DeckLists);
	
	if ($conn->query($query_DeckLists) === TRUE) {
		echo('<br>Deck list table successfully updated.');
	}
}

if (!empty($query_DeckStats)) {
	$query_DeckStats = 'INSERT INTO DeckStats VALUES ' . $query_DeckStats;
	echo('<br><br>' . $query_DeckStats);
	
	if ($conn->query($query_DeckStats) === TRUE) {
		echo('<br>Deck list table successfully updated.');
	}
}

$conn->close();

?>