<?php

$API_token = 'enter your API Key';

/*************************
 *  DataBase Connection  *
 *************************/
 
$sql_serveur = '127.0.0.1';
$sql_username = 'root';
$sql_password = '';
$sql_dbname = 'ClashRoyale';
 
$conn = new mysqli($sql_serveur,$sql_username,$sql_password,$sql_dbname);

// Check connection
if ($conn->connect_errno) {
    printf("Connection failed: %s\n", $conn->connect_error);
    exit();
}

/***********************************
 *  DataBase Preliminary requests  *
 ***********************************/
 
$query = 'SELECT createdDate FROM ClanWar';
$query_result = $conn->query($query);

while($row = $query_result->fetch_array(MYSQLI_ASSOC)) {
	$warList[] = $row['createdDate'];
}
$query_result->free();

/******************
 *  API requests  *
 ******************/

$opts = array(
  'http'=>array(
    'method'=>'GET',
    'header'=>'auth: enter your API Key',
	'timeout'=>30
  )
);

$context = stream_context_create($opts);

$clan_id = '2P0V8GL';
 
$warlog_json_response = file_get_contents('https://api.royaleapi.com/clan/' . $clan_id . '/warlog', true, $context);
$warlog_response = json_decode($warlog_json_response);



/*********************************
 *  Request responses treatment  *
 *********************************/

// Clan war update

$query_clanwar = '';
$query_war_participants = '';
$query_war_standings = '';
$query_war_clan_badges = '';

foreach($warlog_response as $war) {
	$already_exist = FALSE;
	if (!empty($warList)) { 								// Check if the war list in DB is not empty
		if (in_array($war->createdDate,$warList)) { 		// Check if the clan war already exist in DB
			$already_exist = TRUE;
			echo('<br>Clan war already exists.');
		} else {
			echo('<br>New clan war found.');
		}
	}
	
	if (!$already_exist) {
		// Clan war table		
		if (!empty($query_clanwar)) {
			$query_clanwar = $query_clanwar . ', ';
		}
		$query_clanwar = $query_clanwar . '(' . $war->createdDate . ', ' . $war->seasonNumber . ')';
		
		// Participants table
		foreach($war->participants as $part) {
			if (!empty($query_war_participants)) {
				$query_war_participants = $query_war_participants . ', ';
			}
			$query_war_participants = $query_war_participants . '(' . $war->createdDate . ', "' . $part->tag . '", "' . mysql_real_escape_string($part->name) . '", ' . $part->cardsEarned . ', ' . $part->battlesPlayed . ', ' . $part->wins . ')';
		}
		
		// standings table
		foreach($war->standings as $stand) {
			if (!empty($query_war_standings)) {
				$query_war_standings = $query_war_standings . ', ';
				$query_war_clan_badges = $query_war_clan_badges . ', ';
			}
			$query_war_standings = $query_war_standings . '(' . $war->createdDate . ', "' . $stand->tag . '", "' . mysql_real_escape_string($stand->name) . '", ' . $stand->participants . ', ' . $stand->battlesPlayed . ', ' . $stand->wins . ', ' . $stand->crowns . ', ' . $stand->warTrophies . ', ' . $stand->warTrophiesChange . ')';
			
			$badge = $stand->badge;
			$query_war_clan_badges = $query_war_clan_badges . '(' . $war->createdDate . ', "' . $stand->tag . '", "' . mysql_real_escape_string($badge->name) . '", "' . $badge->category . '", ' . $badge->id . ', "' . $badge->image . '")';
		}
	}
}

echo('<br>');

if (!empty($query_clanwar)) {
	$query_clanwar = 'INSERT INTO clanwar VALUES ' . $query_clanwar;
	
	if ($conn->query($query_clanwar) === TRUE) {
		echo('<br>Clan war table successfully updated.');
	}
}

if (!empty($query_war_participants)) {
	$query_war_participants = 'INSERT INTO participants VALUES ' . $query_war_participants;
	
	if ($conn->query($query_war_participants) === TRUE) {
		echo('<br>Participants table successfully updated.');
	}
}

if (!empty($query_war_standings)) {
	$query_war_standings = 'INSERT INTO standings VALUES ' . $query_war_standings;
	
	if ($conn->query($query_war_standings) === TRUE) {
		echo('<br>Standings table successfully updated.');
	}
}

if (!empty($query_war_clan_badges)) {
	$query_war_clan_badges = 'INSERT INTO ClanWarBadges VALUES ' . $query_war_clan_badges;
	
	if ($conn->query($query_war_clan_badges) === TRUE) {
		echo('<br>War clan badges table successfully updated.');
	}
}

$conn->close();

?>